# -*- coding: utf-8 -*-
"""
Created on Tuesday, May 22, 2019
Author: Bjorn Stevens (bjorn.stevens@mpimet.mpg.de)
"""
#
import numpy as np
from scipy import interpolate, optimize

gravity = 9.8076

cpd     = 1006.
Rd      = 287.05

Rv      = 461.53    # IAPWS97 at 273.15
cpv     = 1865.01   # ''
cpl     = 4219.32   # '' 
cpi     = 2096.70   # ''
lv0     = 2500.93e3 # ''
lf0     =  333.42e3 #''

eps1     = Rd/Rv
eps2     = Rv/Rd -1.


P0      = 100000.  # Standard Pressure [Pa]
T0      = 273.15   # Standard Temperature [K]
e0      = 611.2    # This is the saturation vapor pressure at T0 [K]
sd00    = 6783     # Dry air entropy at P0, T0
sv00    = 10321    # Water vapor entropy at P0, T0
PvC     = 22.064e6 # Critical pressure [Pa] of water vapor
TvC     = 647.096  # Critical temperature [K] of water vapor
TvT     = 273.16   # Triple point temperature [K] of water
PvT     = 611.655
#T_ice   = 233.15
T_ice   = 240.15 #below -33 C only ice